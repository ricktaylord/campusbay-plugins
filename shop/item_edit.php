<?php
            // This lines prevent to clear the field if the form is reloaded
            if( Session::newInstance()->_getForm('shop_item_quality') != '' ) {
                $detail['shop_item_quality'] = Session::newInstance()->_getForm('shop_item_quality');
            }
        ?>

<div class="box"
<div class="box dg_files">
<label for="shop_item_quality"><?php _e('Item quality', 'shop'); ?></label>

<fieldset>
<select name="shop_item_quality" id="shop_item_quality">
	<?php foreach(@$detail['a_shop_item_quality_options'] as $row)  { ?>
	
	<option value="<?php echo $row['pk_i_id'] ?>" 
	<?php if(isset($detail['shop_item_quality']) && $detail['shop_item_quality']['pk_i_id']==$row['pk_i_id']) { ?> selected='selected'<?php }?>
        ><?php _e($row['s_name'], 'shop') ?></option>
	<?php } ?>
</select>


<h3>Preferred payment options</h3>
<input style="width: 30px;" type="checkbox" name="shop_accept_paypal" id="shop_accept_paypal" value="1" <?php if(@$detail['b_accept_paypal'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="shop_accept_paypal"><?php _e('Paypal', 'shop'); ?></label>

<input style="width: 30px;" type="checkbox" name="shop_accept_google_checkout" id="shop_accept_google_checkout" value="1" <?php if(@$detail['b_accept_google_checkout'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="shop_accept_bank_google_checkout"><?php _e('Google Checkout', 'shop'); ?></label> <br />
<input style="width: 30px;" type="checkbox" name="shop_accept_cheque" id="shop_accept_cheque" value="1" <?php if(@$detail['b_accept_cheque'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="shop_accept_cheque"><?php _e('Cheque', 'shop'); ?></label>        
<input style="width: 30px;" type="checkbox" name="shop_accept_bank_transfer" id="shop_accept_bank_transfer" value="1" <?php if(@$detail['b_accept_bank_transfer'] == 1) { echo 'checked="yes"'; } ?>/> <label style="float:none;" for="shop_accept_bank_transfer"><?php _e('Bank transfer', 'shop'); ?></label>        

	</fieldset>
	    </div>
</div>
