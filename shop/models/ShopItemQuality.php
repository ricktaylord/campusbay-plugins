<?php
class ShopItemQuality extends InstallableDAO {
       private static $instance ;
       private static $initial_values ;

        /**
         * It creates a new ShopItem object class if if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since unknown
         * @return Currency
         */
        public static function newInstance() {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

	public static function getInitialValues() {
		if(!self::$initial_values) {
		   self::$initial_values = array('As new','Very good','Good','OK','Poor');
		}
		return self::$initial_values;
	}

	public function insertInitialValues() {
		foreach(self::getInitialValues() as $nm)	
		{
			$this->insert(array('s_name'=>$nm));
		}
	}

        /**
         * Construct
         */
        function __construct() {
            parent::__construct();
            $this->setTableName('t_shop_item_quality') ;
            $this->setPrimaryKey('pk_i_id') ;
            $this->setFields( array('pk_i_id', 's_name') ) ;
        }	

}
