<?php
class ShopFavs extends InstallableDAO {
       private static $instance ;

        /**
         * It creates a new ShopFav object class if if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since unknown
         * @return Currency
         */
        public static function newInstance() {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }
	
        /**
         * Construct
         */
        function __construct() {
            parent::__construct();
            $this->setTableName('t_shop_favs') ;
            $this->setPrimaryKey(array('fk_i_user_id','fk_i_seller_id')) ;
	    $this->setFields(array('fk_i_user_id','fk_i_seller_id'));
        }	


}
