<?php
class ShopUser extends InstallableDAO {
       private static $instance ;

        /**
         * It creates a new ShopItem object class if if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since unknown
         * @return Currency
         */
        public static function newInstance() {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }
	
	public function incrementSalesByPrimaryKey($id) {
		$this->dao->set('i_total_sales', '(i_total_sales + 1)');
		$this->dao->where($this->getPrimaryKey(), $id);
		$affected = $this->dao->update();
		return $affected===false?false:true;
	}
	public function incrementBuysByPrimaryKey($id) {
		$this->dao->set('i_total_buys', '(i_total_buys + 1)');
		$this->dao->where($this->getPrimaryKey(), $id);
		$affected = $this->dao->update();
		return $affected===false?false:true;
	}

        /**
         * Construct
         */
        function __construct() {
            parent::__construct();
            $this->setTableName('t_shop_user') ;
            $this->setPrimaryKey('fk_i_user_id') ;
            $this->setFields( array('fk_i_user_id', 'i_amount', 'b_accept_paypal', 'b_accept_bank_transfer') ) ;
        }	

}
