<?php
class ShopItem extends InstallableDAO {
       private static $instance ;
       private static $payment_option_meta;

        /**
         * It creates a new ShopItem object class if if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since unknown
         * @return Currency
         */
        public static function newInstance() {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

	public static function getPaymentOptionMeta() {
		if(!self::$payment_option_meta) {
		   self::$payment_option_meta = array(
		       'paypal'=>array('key'=>'b_accept_paypal', 'instructions'=>'paypal_instructions.php', 'param'=>'shop_accept_paypal'),
		       'google_checkout'=>array('key'=>'b_accept_google_checkout', 'instructions'=>'google_checkout_instructions.php', 'param'=>'shop_accept_google_checkout'),
		       'bank_transfer'=>array('key'=>'b_accept_bank_transfer', 'instructions'=>'bank_transfer_instructions.php', 'param'=>'shop_accept_bank_transfer'),
                       'cheque'=>array('key'=>'b_accept_cheque', 'instructions'=>'cheque_instructions.php', 'param'=>'shop_accept_cheque'),
                       
		   );
		}
		return self::$payment_option_meta;
	}

        /**
         * Construct
         */
        function __construct() {
            parent::__construct();
            $this->setTableName('t_shop_item') ;
            $this->setPrimaryKey('fk_i_item_id') ;
            $this->setFields( array(	'fk_i_item_id', 
				   	'i_amount',
					'fk_i_quality_id', 
					'b_digital', 
					'b_accept_paypal', 
					'b_accept_bank_transfer',
					'b_accept_cheque',
					'b_accept_google_checkout',
					) ) ;
        }	

}
