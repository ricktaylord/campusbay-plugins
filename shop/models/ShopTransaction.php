<?php
class ShopTransaction extends InstallableDAO {
       private static $instance ;
       private $latestId ;

        /**
         * It creates a new ShopTransaction object class if if it has been created
         * before, it return the previous object
         * 
         * @access public
         * @since unknown
         * @return Currency
         */
        public static function newInstance() {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }
	
        /**
         * Construct
         */
        function __construct() {
            parent::__construct();
            $this->setTableName('t_shop_transactions') ;
            $this->setPrimaryKey('pk_i_id') ;
            $this->setFields( array('pk_i_id','fk_i_item_id', 'fk_i_user_id','fk_i_buyer_id', 'i_amount', 'i_buyer_score', 'i_seller_score', 's_buyer_comment', 's_seller_comment', 's_code', 'f_item_price', 's_currency', 'e_status') ) ;
        }	

	protected function getScores($score_field) {
		$this->dao->select("fk_i_buyer_id AS pk_i_id, SUM(".$score_field.") AS i_score, COUNT(*) AS i_transactions");
		$this->dao->from($this->getTableName());
		$this->dao->where('e_status', 'ENDED');
		$this->dao->groupBy('fk_i_buyer_id');
		$result = $this->dao->get();
		return $result->resultArray();
	}
	public function getBuyerScores() {
		return $this->getScores("i_buyer_score");
	}
	public function getSellerScores() {
		return $this->getScores("i_seller_score");
	}

	private function setLatestId($id) {
		$this->latestId = $id;
	}

	public function getLatestId() {
                return $this->latestId;
	}
	public function insert($values) {
		parent::insert($values);
		$this->setLatestId($this->dao->insertedId());
	 	ShopLog::newInstance()->insert(array('fk_i_transaction_id'=>$this->getLatestId(), 'e_status'=>$values['e_status'], 'fk_i_user_id'=>$values['fk_i_user_id'], date('Y-m-d H:i:s')));

	}

}
