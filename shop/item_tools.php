<?php if( ! osc_is_web_user_logged_in()) { ?>
	<h3>Buy now</h3>
	<a href="<?php echo osc_user_login_url() ?>">Log in</a> or <a href="<?php echo osc_register_account_url()?>">register for free</a> to get in touch with the seller.
<?php } elseif(( osc_logged_user_id() == osc_item_user_id() ) && osc_logged_user_id() != 0 ) { ?>

<?php } elseif( shop_item_is_for_sale()) { ?>

<div class="box">
    <div class="box dg_files">
        <form method="POST" action="<?php echo osc_base_url(true); ?>">
            <input type="hidden" name="item_id" value="<?php echo osc_item_id(); ?>" />
            <input type="hidden" name="page" value="custom" />
            <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__) ?>confirm_buy.php" />
            <div><?php if(isset($detail) && isset($detail['i_amount']) && $detail['i_amount']>0) {
                if($detail['i_amount']>1) { ?>
                    <input type="text" name="shop_amount" value="1" onKeyPress="return numbersonly(this, event)"/> <?php echo sprintf(__('of %d items', 'shop'), $detail['i_amount'])?>
                <?php } ?>
                    <br />
                    <input type="submit" value="<?php _e('Buy!', 'shop')?>" />
            <?php } else { ?>
                <strong class="sold"><?php _e('This item has been sold', 'shop'); ?></strong>
            <?php }; ?></div>
        </form>
    </div>
</div>
<?php } ?>
<SCRIPT TYPE="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

</SCRIPT>
