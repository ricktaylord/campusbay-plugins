<div class="box">
	<h3> Payment options</h3>

            <?php if(shop_item_has_payment_options($detail)) { ?>
            	    <ul><li><?php echo implode("</li><li>", shop_item_payment_options($detail)); ?></li></ul>
	    <?php } else { ?>
		<p>The seller has not specified any payment options so ask them for details.</p>
	    <?php }; ?>
	<h3> Terms and conditions</h3>

	    <?php if(shop_item_has_tncs($detail)) { ?>

		   <p><?php shop_item_tncs($detail); ?></p>
	     <?php } else { ?>
		  None specified.  Please ask the seller.
	     <?php }; ?>
</div>
<SCRIPT TYPE="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

</SCRIPT>
