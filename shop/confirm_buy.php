<?php View::newInstance()->_exportVariableToView('item', Item::newInstance()->findByPrimaryKey(Params::getParam('item_id')));
$conn = getConnection();
$detail = $conn->osc_dbFetchResult("SELECT * FROM %st_shop_item WHERE fk_i_item_id = %d", DB_TABLE_PREFIX, osc_item_id());
$amount = min(Params::getParam('shop_amount')!=''?Params::getParam('shop_amount'):1, $detail['i_amount']);
if($amount<0) { $amount = 1; }; ?>
<div style="width:50%; float:left; height:150px;">
    <div class="odd">
        <?php if( osc_images_enabled_at_items() ) { ?>
         <div class="photo">
             <?php if(osc_count_item_resources()) { ?>
                <a href="<?php echo osc_item_url() ; ?>"><img src="<?php echo osc_resource_thumbnail_url() ; ?>" width="75px" height="56px" title="" alt="" /></a>
            <?php } else { ?>
                <img src="<?php echo osc_current_web_theme_url('images/no_photo.gif') ; ?>" title="" alt="" />
            <?php } ?>
         </div>
         <?php } ?>
         <div class="text">
             <h3>
                 <a href="<?php echo osc_item_url() ; ?>"><?php echo osc_item_title() ; ?></a>
             </h3>
             <p>
                 <sdivong><?php if( osc_price_enabled_at_items() ) { echo osc_item_formated_price() ; ?> - <?php } echo osc_item_city(); ?> (<?php echo osc_item_region(); ?>) - <?php echo osc_format_date(osc_item_pub_date()); ?></sdivong>
             </p>
             <p><?php echo osc_item_description(); ?></p>
         </div>
     </div>
</div>
<div style="width:50%; float:left; height:150px;">
    <?php if(osc_is_web_user_logged_in()) {
        echo sprintf(__('You are going to tell the seller that you want to buy %d units of %s at a total price of %s %s.  You will pass your email address to the seller.', 'shop'), $amount, osc_item_title(), ($amount*  osc_item_formated_price()), osc_item_currency()); ?><br />
        <?php _e('This action can not be undone.  Once you\'ve done it, the item will be removed from public display on the site.  You will be shown your payment options, and the seller will be in touch.', 'shop'); ?><br />
        <form action="<?php echo osc_base_url(true)?>" method="POST" >
            <input type="hidden" name="page" value="custom" />
            <input type="hidden" name="file" value="<?php echo osc_plugin_folder(__FILE__); ?>paying.php" />
            <input type="hidden" name="bought" value="true" />
            <input type="hidden" name="item_id" value="<?php echo osc_item_id(); ?>" />
            <input type="hidden" name="shop_amount" value="<?php echo $amount; ?>" />
            <input type="submit" value="<?php _e('Buy this item','shop')?>" />
        </form>
    <?php } else {
        _e('You need to login in order to tell the seller that you want to buy this item', 'shop');
        ?>
        <form id="login" action="<?php echo osc_base_url(true) ; ?>" method="post">
            <fieldset>
                <input type="hidden" name="page" value="login" />
                <input type="hidden" name="action" value="login_post" />
                <input type="hidden" name="http_referer" value="<?php echo osc_base_url(true)."?page=custom&file=".osc_plugin_folder(__FILE__)."paying.php&item_id=".osc_item_id()."&shop_amount=".$amount; ?>" />
                <label for="email"><?php _e('E-mail', 'modern') ; ?></label>
                <?php UserForm::email_login_text() ; ?>
                <label for="password"><?php _e('Password', 'modern') ; ?></label>
                <?php UserForm::password_login_text() ; ?>
                <p class="checkbox"><?php UserForm::rememberme_login_checkbox();?> <label for="rememberMe"><?php _e('Remember me', 'modern') ; ?></label></p>
                <button type="submit"><?php _e('Log in', 'modern') ; ?></button>
                <div class="forgot">
                    <a href="<?php echo osc_recover_user_password_url() ; ?>"><?php _e("Forgot password?", 'modern');?></a>
                </div>
            </fieldset>
        </form>
    <?php }; ?>
</div>
