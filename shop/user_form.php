<h3>Receiving payments as a seller</h3>
<p>(If you don't want to sell anything, or if you don't want to receive online payments, don't worry about entering anything here.)</p>
<label for="shop_paypal_id">Your Paypal merchant ID</label>
<input name="paypal_id" id="paypal_id" value="" />
<label for="shop_google_checkout_id">Your Google Checkout merchant ID</label>
<input name="google_checkout_id" id="google_checkout_id" value="" />
