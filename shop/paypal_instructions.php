<?php
                $ENDPOINT     = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//                $ENDPOINT     = 'https://www.paypal.com/cgi-bin/webscr';

/*                $r = rand(0,1000);
                $rpl = osc_item_id()."|".$amount."|".osc_item_price()."|".osc_item_currency()."|".$r;
                $RETURNURL = osc_base_url(true) . '?page=custom&file=' . osc_plugin_folder(__FILE__) . 'return.php?rpl=' . $rpl;
                $CANCELURL = osc_base_url(true) . '?page=custom&file=' . osc_plugin_folder(__FILE__) . 'cancel.php?rpl=' . $rpl;
                $NOTIFYURL = osc_base_url(true) . '?page=custom&file=' . osc_plugin_folder(__FILE__) . 'notify_url.php?rpl=' . $rpl;
*/
		$RETURNURL = osc_item_url();
?>

<?php echo sprintf(__('Seller accepts Paypal as payment, click the button below or make a Paypal payment manually with the code: "%s"', 'shop'), $txn_code); ?>
              <br /><br />
	      <form action="<?php echo $ENDPOINT; ?>" method="post" id="payment_<?php echo $r; ?>">
                  <input type="hidden" name="cmd" value="_xclick" />
                  <input type="hidden" name="upload" value="1" />
                  <input type="hidden" name="business" value="<?php echo shop_item_paypal_merchant_id(); ?>" />
                  <input type="hidden" name="item_name" value="<?php echo osc_item_title(); ?>" />
                  <input type="hidden" name="item_number" value="<?php echo $transaction; ?>" />
                  <input type="hidden" name="amount" value="<?php echo osc_item_formated_price(); ?>" />
                  <input type="hidden" name="quantity" value="<?php echo $amount; ?>" />

                  <input type="hidden" name="currency_code" value="<?php echo osc_item_currency(); ?>" />
                  <input type="hidden" name="rm" value="2" />
                  <input type="hidden" name="no_note" value="1" />
                  <input type="hidden" name="charset" value="utf-8" />
                  <input type="hidden" name="return" value="<?php echo $RETURNURL; ?>" />
<!--                  <input type="hidden" name="notify_url" value="<?php echo $NOTIFYURL; ?>" />  -->
                  <input type="hidden" name="cancel_return" value="<?php echo $CANCELURL; ?>" />
<!--                  <input type="hidden" name="custom" value="<?php echo $rpl; ?>" /> -->
                </form>
                <div class="buttons">
                  <div class="right"><a id="button-confirm" class="button" onclick="$('#payment_<?php echo $r; ?>').submit();"><span><img src='<?php echo osc_base_url().'oc-content/plugins/'.osc_plugin_folder(__FILE__); ?>paypal.gif' border='0' /></span></a></div>
                </div>

