<?php
/*
Plugin Name: Shop
Plugin URI: http://www.osclass.org/
Description: This plugin transforms your OSClass into a shop!
Version: 1.0
Author: OSClass
Author URI: http://www.osclass.org/
Short Name: shop
*/
    require_once('models/InstallableDAO.php');
    require_once('models/ShopItem.php');
    require_once('models/ShopItemQuality.php');
    require_once('models/ShopUser.php');
    require_once('models/ShopTransaction.php');
    require_once('models/ShopLog.php');
    require_once('models/ShopPaypalLog.php');
    require_once('models/ShopFavs.php');
    require_once('models/ShopMessage.php');
    function shop_install() {
	$db = getConnection();
	$db->autocommit(false);
        try {
            // import struct
            $path = osc_plugin_resource('shop/struct.sql');
            $sql = file_get_contents($path);
           $db->osc_dbImportSQL($sql);

            // create shop extension table entries for existing users
            $users = User::newInstance()->listAll();
//            $shop_user = array('f_score'=>0, 'i_total_sales'=>0, 'i_total_buys'=>0);
            $shop_user = array();
            foreach($users as $user) {
		$shop_user['fk_i_user_id'] = $user['pk_i_id'];
		ShopUser::newInstance()->insert($shop_user);
            }

            // create shop extension table entries for existing items
            $items = Item::newInstance()->listAllWithCategories();
	    $shop_item = array();
            foreach($items as $item) {
		$shop_item['fk_i_item_id'] = $item['pk_i_id'];
                //$conn->osc_dbExec("INSERT INTO %st_shop_item (fk_i_item_id, i_amount, b_digital, b_accept_paypal, b_accept_bank_transfer) values (%d, 0, 0, 0, 0)", DB_TABLE_PREFIX, $item['pk_i_id']);
            }

	    //set up item quality initial vals

	    ShopItemQuality::newInstance()->insertInitialValues();
	    // set up email templates as pages
	    $desc = array();
	    $page = array('b_indelible'=>1);
	    $desc[osc_language()] = array ('s_title'=>'{WEB_TITLE} - You want to buy {ITEM_TITLE} ({TXN_CODE})',
					   's_text'=>'<p>You\'ve told the seller, via {WEB_TITLE}, that you want to buy this item for {PRICE} (transaction #ID: {TXN_CODE}).</p>\r\n<p> The seller will be in touch by email to arrange getting the item to you.</p><p>To pay for it, follow these instructions : {INSTRUCTIONS}</p>\r\n<p>Thanks</p>');
	    $page['s_internal_name'] = 'email_shop_sold_buyer';
	    Page::newInstance()->insert($page,$desc);
	    $desc[osc_language()]['s_title'] = '{WEB_TITLE} - Your item {ITEM_TITLE} has an interested buyer';
	    $desc[osc_language()]['s_text'] = '<p>Dear {CONTACT_NAME},</p>\r\n<p> </p>\r\n<p>Good news!  We just found a buyer for your item: ({ITEM_TITLE})</p>\r\n<p>They are {USER_NAME} and their email address is {USER_EMAIL}.  You should now email them to arrange getting the item to them and completing the sale.</p>\r\n<p>Your item has now been taken down from public display on {WEB_TITLE}.  If for any reason the sale with {USER_NAME} falls through, you can repost your item on {WEB_TITLE} by clicking the following link: {ITEM_REPOST_URL}</p><p>Thanks for using {WEB_TITLE}</p>';
            $page['s_internal_name'] = 'email_shop_sold_seller';
            Page::newInstance()->insert($page,$desc);
            $desc[osc_language()]['s_title'] = '{WEB_TITLE} - Someone has a question';
            $desc[osc_language()]['s_text'] = '<p>Dear {CONTACT_NAME},</p>\n<p>{USER_NAME} ({USER_EMAIL}) left you a message :</p>\n<p>{COMMENT}</p>\n<p>Thank you for using </p>\n<p>{WEB_TITLE}</p>';
	    $page['s_internal_name'] = 'email_shop_contact';
            Page::newInstance()->insert($page,$desc);
            $db->commit();
            $db->autocommit(false);
        } catch (DatabaseException $e) {
            $db->rollback();
	    $db->autocommit(true);
            throw($e);
        }
    }

    function shop_uninstall() {
	$db = getConnection();
	$db->autocommit(false);
        try {
            ShopItem::newInstance()->uninstall();
	    ShopUser::newInstance()->uninstall();
            ShopTransaction::newInstance()->uninstall();
            ShopLog::newInstance()->uninstall();
            ShopPaypalLog::newInstance()->uninstall();
            ShopFavs::newInstance()->uninstall();
            ShopMessage::newInstance()->uninstall();
            ShopItemQuality::newInstance()->uninstall();
            /*
            $conn->osc_dbExec('DROP TABLE %st_shop_item', DB_TABLE_PREFIX);
            $conn->osc_dbExec('DROP TABLE %st_shop_user', DB_TABLE_PREFIX);
            $conn->osc_dbExec('DROP TABLE %st_shop_transactions', DB_TABLE_PREFIX);
            $conn->osc_dbExec('DROP TABLE %st_shop_log', DB_TABLE_PREFIX);
            $conn->osc_dbExec('DROP TABLE %st_shop_paypal_log', DB_TABLE_PREFIX);
            $conn->osc_dbExec('DROP TABLE %st_shop_favs', DB_TABLE_PREFIX);
            $conn->osc_dbExec('DROP TABLE %st_shop_message', DB_TABLE_PREFIX);
            */
            // delete email template pages
	    Page::newInstance()->deleteByInternalName('email_shop_sold_buyer');
            Page::newInstance()->deleteByInternalName('email_shop_sold_seller');
            Page::newInstance()->deleteByInternalName('email_shop_contact');
            $db->commit();
            $db->autocommit(true);
        } catch (DatabaseException $e) {
            $db->rollback();
            $db->autocommit(true);
	    throw($e);
        }
    }

    function shop_item_process_transaction($amount = 1) {
	$shop_item = ShopItem::newInstance()->findByPrimaryKey(osc_item_id()); 
        if(!isset($shop_item['i_amount'])) 
		return false;

            if($amount>($shop_item['i_amount'] + 0)) {
                $amount = ($shop_item['i_amount'] + 0);
            }
           
                $txn_code = strtoupper(osc_genRandomPassword(12));
		$dbTransaction = array('fk_i_item_id'=>osc_item_id(),
			       'fk_i_user_id'=>osc_item_user_id(),
                               'fk_i_buyer_id'=>osc_logged_user_id(),
                               'i_amount'=>$amount,
                               'f_item_price'=>osc_item_formated_price(),
                               's_currency'=>osc_item_currency(),
                               'e_status'=>'SOLD',
                               's_code'=>$txn_code);
		$db = getConnection();
		$db->autocommit(false);
		try {
		//record transaction (and log)
		$mTxn = ShopTransaction::newInstance();
		
	        $mTxn->insert($dbTransaction);

		//$conn->osc_dbExec("INSERT INTO %st_shop_transactions (fk_i_item_id, fk_i_user_id, fk_i_buyer_id, i_amount, f_item_price, s_currency, e_status, s_code) VALUES (%d, %d, %d, %d, %f, '%s', 'SOLD', '%s')", DB_TABLE_PREFIX, osc_item_id(), osc_item_user_id(), osc_logged_user_id(), $amount, osc_item_price(), osc_item_currency(), $txn_code);
		// decrement the shop item amount
		ShopItem::newInstance()->updateByPrimaryKey(array('i_amount'=>$shop_item['i_amount']-$amount), osc_item_id());
		// increment sales / buys for user pair
		ShopUser::newInstance()->incrementSalesByPrimaryKey(osc_item_user_id());
		ShopUser::newInstance()->incrementBuysByPrimaryKey(osc_logged_user_id());
                shop_send_sold_email($mTxn->getLatestId());
		$db->autocommit(true);
		   return true;
		} catch (DatabaseException $e) {
			$db->rollback();
			$db->autocommit(true);	
			throw($e);
		}
	 return false;
    }
    
    function shop_redirect_to($url) {
        header('Location: ' . $url);
        exit;
    }

    function shop_search_conditions($params) {
	// filter out sold items from search
	// this assumes that, in the SQL query, if the amount field is NULL after JOIN, the row is not a shop item.
	/*
	function is_shop_category($id) {
		return osc_is_this_category('shop',$id);
	}
	if(!isset($params['sCategory']))
	{
		$shop_categories = array();
	}
	elseif(is_array($params['sCategory']))
	{
		$shop_categories = array_filter($params['sCategory'],'is_shop_category');
	}
	else
	{
		if(is_shop_category($id))
		{
			$shop_categories = array($id);
		}	
		else
		{
			$shop_categories = array();
		}
	}
	
	if(!empty($shop_categories))
	{
	*/
		Search::newInstance()->addConditions(sprintf("(%st_shop_item.i_amount > 0 OR %st_shop_item.i_amount IS NULL)", DB_TABLE_PREFIX, DB_TABLE_PREFIX));
//		Search::newInstance()->addConditions(sprintf("(%st_item.pk_i_id = %st_shop_item.fk_i_item_id OR %st_shop_item.i_amount IS NULL) ", DB_TABLE_PREFIX, DB_TABLE_PREFIX));
//        	Search::newInstance()->addTable(sprintf("%st_shop_item", DB_TABLE_PREFIX));
		Search::newInstance()->dao->join(sprintf("%st_shop_item",  DB_TABLE_PREFIX), sprintf("%st_item.pk_i_id = %st_shop_item.fk_i_item_id",DB_TABLE_PREFIX,DB_TABLE_PREFIX), "LEFT"); 
	//}

    }
    
    function shop_configure_link() {
        osc_plugin_configure_view(osc_plugin_path(__FILE__) );
    }
    

    function shop_form($catId = '') {
	if($catId!='')
	{
		if(osc_is_this_category('shop', $catId)) {
			$detail['i_amount'] = 1;
			$conn = getConnection();
			$detail['a_shop_item_quality_options'] = ShopItemQuality::newInstance()->listAll();
		
			 require_once 'item_edit.php';
		}
	}
    }

    function shop_item_detail() {
	if(osc_is_this_category('shop', osc_item_category_id())) {
		$detail = ShopItem::newInstance()->findByPrimaryKey(osc_item_id());
		// can be loaded twice on the page?
       		 require 'item_detail.php';
	}
    }


    function shop_item_tools() {
	$detail = ShopItem::newInstance()->findByPrimaryKey(osc_item_id());
	require 'item_tools.php';
    }

    function shop_get_item_payment_option_meta()  {
        static $opts;
        if($opts) return $opts;
        else {
	   $opts = array(
               'paypal'=>array('key'=>'b_accept_paypal', 'instructions'=>'paypal_instructions.php', 'param'=>'shop_accept_paypal'),
               'bank_transfer'=>array('key'=>'b_accept_bank_transfer', 'instructions'=>'bank_transfer_instructions.php', 'param'=>'shop_accept_bank_transfer')
           );
        }
	return $opts;
    }


    function shop_item_edit($catId = null, $item_id = null) {
	if($catId!=null)
	{
		if(osc_is_this_category('shop', $catId)) {


	$detail = ShopItem::newInstance()->findByPrimaryKey(osc_item_id());
        require_once 'item_edit.php';
	}
	}
    }

    function shop_item_edit_post($catId = null, $item_id = null) {
	if($catId!=null) 
	{
		if(osc_is_this_category('shop',$catId))
		{
			$toDB=array();
			// include payment options from param
			foreach(ShopItem::getPaymentOptionMeta() as $k=>$v)
			{
				$toDB[$v['key']] = Params::getParam($v['param']);
			}
			// include amount from param
			//$amount = Params::getParam('shop_amount')>=0?Params::getParam('shop_amount'):0;
			$amount = "1";
			$toDB['i_amount'] = $amount;
			// include quality from param
			$toDB['fk_i_quality_id'] = Params::getParam('shop_item_quality')+0;
			// does the item exist?
			$item = ShopItem::newInstance()->findByPrimaryKey($item_id);
			 if($item===false) {
			    // if not, insert
			    $toDB['fk_i_item_id'] = $item_id;
			    ShopItem::newInstance()->insert($toDB);
			} else {
			    // otherwise, update.
			    ShopItem::newInstance()->updateByPrimaryKey($toDB, $item_id);
			}
		}
	}
    }

    function shop_delete_item($item) {
	ShopItem::newInstance()->deleteByPrimaryKey($item);
    }
    
    function shop_user_menu() {
        echo '<li class="opt_shop" ><a href="' . osc_render_file_url(osc_plugin_folder(__FILE__)."menu_buyer.php") . '" >' . __("Items bought", "shop") . '</a></li>' ;
        echo '<li class="opt_shop" ><a href="' . osc_render_file_url(osc_plugin_folder(__FILE__)."menu_seller.php") . '" >' . __("Items sold", "shop") . '</a></li>' ;
        echo '<li class="opt_shop" ><a href="' . osc_render_file_url(osc_plugin_folder(__FILE__)."favorites.php") . '" >' . __("Favourite sellers", "shop") . '</a></li>' ;
    }

    function shop_user_form() {
	require_once('user_form.php');
    }
    
    function shop_profile_link($id = NULL) {
        if($id==NULL || $id=='') {
            $id = osc_user_id();
            if($id=='') {
                $id = osc_item_user_id();
            }
        }
        if($id!='') {
            return osc_render_file_url(osc_plugin_folder(__FILE__)."profile.php&user_id=").$id;
        } else {
            return '';
        }
    }
    
    
    function shop_send_contact_email($from, $to, $msg, $item_id) {
        if($item_id!='') {
            $aItem = array(
                'id' => $item_id
                ,'yourEmail' => $from['s_email']
                ,'yourName' => $from['s_name']
                ,'message' => $msg
            );
            fn_email_item_inquiry($aItem);
        } else {
            Page::newInstance()->findByInternalName('email_shop_contact');
            $locale = osc_current_user_locale() ;

            $content = array();
            if(isset($aPage['locale'][$locale]['s_title'])) {
                $content = $aPage['locale'][$locale];
            } else {
                $content = current($aPage['locale']);
            }

            $words   = array();
            $words[] = array('{CONTACT_NAME}', '{USER_NAME}', '{USER_EMAIL}',
                                 '{WEB_URL}', '{COMMENT}');

            $words[] = array($to['s_name'], $from['s_name'], $from['s_email'],
                             osc_base_url(), $msg );

            $title = osc_mailBeauty(osc_apply_filter('email_title', osc_apply_filter('email_shop_contact_title', $content['s_title'])), $words);
            $body = osc_mailBeauty(osc_apply_filter('email_description', osc_apply_filter('email_shop_conatct_description', $content['s_text'])), $words);

            $from_email = osc_contact_email() ;
            $from_name = osc_page_title() ;

            $emailParams = array (
                                'from'      => $from_email
                                ,'from_name' => $from_name
                                ,'subject'   => $title
                                ,'to'        => $to['s_email']
                                ,'to_name'   => $to['s_name']
                                ,'body'      => $body
                                ,'alt_body'  => $body
                                ,'reply_to'  => $from
                            ) ;

            osc_sendMail($emailParams);
        }
    }
    
    function shop_send_sold_email($txn_id) {
	$txn = ShopTransaction::newInstance()->findByPrimaryKey($txn_id);
	$txn_code = $txn['s_code'];
	
        //$txn = $conn->osc_dbFetchResult("SELECT * FROM %st_shop_transactions WHERE pk_i_id = %d", DB_TABLE_PREFIX, $txn_id);
        $item = Item::newInstance()->findByPrimaryKey($txn['fk_i_item_id']);
        View::newInstance()->_exportVariableToView('item', $item);
        $seller = User::newInstance()->findByPrimaryKey($txn['fk_i_user_id']);
        $buyer = User::newInstance()->findByPrimaryKey($txn['fk_i_buyer_id']);
	$shop_item = ShopItem::newInstance()->findByPrimaryKey($txn['fk_i_item_id']);
        //$shop_item = $conn->osc_dbFetchResult("SELECT * FROM %st_shop_item WHERE fk_i_item_id = %d", DB_TABLE_PREFIX, $txn['fk_i_item_id']);
        
        $item_url = osc_item_url();
        $item_url = '<a href="'.$item_url.'" >'.$item_url.'</a>';
        $from = osc_contact_email() ;
        $from_name = osc_page_title() ;


        // EMAIL TO BUYER
        $aPage = Page::newInstance()->findByInternalName('email_shop_sold_buyer');
        $locale = osc_current_user_locale() ;
        $content = array();
        if(isset($aPage['locale'][$locale]['s_title'])) {
            $content = $aPage['locale'][$locale];
        } else {
            $content = current($aPage['locale']);
        }

        $price = (osc_prepare_price($item['i_price'])*$txn['i_amount'])." ".$item['fk_c_currency_code'];
        $instructions = '';
        if($shop_item['b_accept_paypal']==1) {
		ob_start();
		require_once('paypal_instructions.php');
		$instructions .= ob_get_contents();
		ob_end_clean();
        };
        if($shop_item['b_accept_bank_transfer']==1) {
		ob_start();
		require_once('bank_transfer_instructions.php');
		$instructions .= ob_get_contents();
		ob_end_clean();
        };
            
        $words   = array();
        $words[] = array('{CONTACT_NAME}', '{USER_NAME}', '{USER_EMAIL}',
                             '{WEB_URL}', '{ITEM_TITLE}','{ITEM_URL}', '{INSTRUCTIONS}','{PRICE}', '{TXN_CODE}');

        $words[] = array($item['s_contact_name'], $buyer['s_name'], $buyer['s_name'],
                         '<a href="'.osc_base_url().'" >'.osc_base_url().'</a>', $item['s_title'], $item_url, $instructions, $price, $txn_code );

        $title = osc_mailBeauty(osc_apply_filter('email_title', osc_apply_filter('email_shop_sold_buyer_title', $content['s_title'])), $words);
        $body = osc_mailBeauty(osc_apply_filter('email_description', osc_apply_filter('email_shop_sold_buyer_description', $content['s_text'])), $words);

        $emailParams = array (
                            'from'      => $from
                            ,'from_name' => $from_name
                            ,'subject'   => $title
                            ,'to'        => $buyer['s_email']
                            ,'to_name'   => $buyer['s_name']
                            ,'body'      => $body
                            ,'alt_body'  => $body
                            ,'reply_to'  => $from
                        ) ;

        osc_sendMail($emailParams);
        
        
        
        // EMAIL TO SELLER
        $aPage = Page::newInstance()->findByInternalName('email_shop_sold_seller');
        $locale = osc_current_user_locale() ;

        $content = array();
        if(isset($aPage['locale'][$locale]['s_title'])) {
            $content = $aPage['locale'][$locale];
        } else {
            $content = current($aPage['locale']);
        }


        $words   = array();
        $words[] = array('{CONTACT_NAME}', '{USER_NAME}', '{USER_EMAIL}',
                             '{WEB_URL}', '{ITEM_TITLE}','{ITEM_URL}', '{BUYER_NAME}');

        $words[] = array($item['s_contact_name'], $seller['s_name'], $seller['s_name'],
                         '<a href="'.osc_base_url().'" >'.osc_base_url().'</a>', $item['s_title'], $item_url, $buyer['s_name']);

        $title = osc_mailBeauty(osc_apply_filter('email_title', osc_apply_filter('email_shop_sold_seller_title', $content['s_title'])), $words);
        $body = osc_mailBeauty(osc_apply_filter('email_description', osc_apply_filter('email_shop_sold_seller_description', $content['s_text'])), $words);

        $emailParams = array (
                            'from'      => $from
                            ,'from_name' => $from_name
                            ,'subject'   => $title
                            ,'to'        => $seller['s_email']
                            ,'to_name'   => $seller['s_name']
                            ,'body'      => $body
                            ,'alt_body'  => $body
                            ,'reply_to'  => $from
                        ) ;

        osc_sendMail($emailParams);
        
    }
    
    
    
    function shop_calculate_scores() {
        $buyers = ShopTransaction::newInstance()->listBuyerScores();
        $sellers = ShopTransaction::newInstance()->listSellerScores();
        $users = array();
        foreach($buyers as $u) {
            $users[$u['pk_i_id']]['score'] = $u['i_score'];
            $users[$u['pk_i_id']]['txn'] = $u['i_transactions'];
        }
        foreach($sellers as $u) {
            if(!isset($users[$u['pk_i_id']])) {
                $users[$u['pk_i_id']]['score'] = $u['i_score'];
                $users[$u['pk_i_id']]['txn'] = $u['i_transactions'];
            } else {
                $users[$u['pk_i_id']]['score'] = $users[$u['pk_i_id']]['score']+$u['i_score'];
                $users[$u['pk_i_id']]['txn'] = $users[$u['pk_i_id']]['txn']+$u['i_transactions'];
            }
        }
        foreach($users as $k => $v) {
            ShopUser::updateByPrimaryKey(array('f_score'=>$v['score']/$v['txn']), $k);
        }
    }
    
    function shop_new_user($user_id) {
	ShopUser::newInstance()->insert(array('fk_i_user_id'=>$user_id));
//        $conn->osc_dbExec("INSERT INTO %st_shop_user (fk_i_user_id, f_score, i_total_sales, i_total_buys) values (%d, 0, 0, 0)", DB_TABLE_PREFIX, $user_id);
    }
    
    function shop_new_item($cat_id, $item_id) {
	ShopItem::newInstance()->insert(array('fk_i_item_id'=>$item_id));
//        $conn->osc_dbExec("INSERT INTO %st_shop_item (fk_i_item_id, i_amount, b_digital, b_accept_paypal, b_accept_bank_transfer) values (%d, 0, 0, 0, 0)", DB_TABLE_PREFIX, $item_id);
    }

function shop_pre_item_post() {
    Session::newInstance()->_setForm('shop_item_quality' , Params::getParam('shop_item_quality'));


    // keep values on session
    Session::newInstance()->_keepForm('shop_item_quality');
}

function shop_item_accepts_paypal($detail)  {
	return (isset($detail['b_accept_paypal']) && $detail['b_accept_paypal']==1);
}

function shop_item_accepts_cheque($detail)  {
	return (isset($detail['b_accept_cheque']) && $detail['b_accept_cheque']==1);
}

function shop_item_accepts_bank_transfer($detail)  {
	return (isset($detail['b_accept_bank_transfer']) && $detail['b_accept_bank_transfer']==1);
}

function shop_item_accepts_google_checkout($detail)  {
	return (isset($detail['b_accept_google_checkout']) && $detail['b_accept_google_checkout']==1);
}


function shop_item_has_payment_options($detail)  {
	return (shop_item_accepts_paypal($detail) || shop_item_accepts_cheque($detail) || shop_item_accepts_bank_transfer($detail) || shop_item_accepts_google_checkout($detail));
}

function shop_item_tncs($detail)  {
	return $detail['s_tncs'];
}

function shop_item_has_tncs($detail) {
	return isset($detail['s_tncs']) && !empty($detail['s_tncs']);
}

function shop_item_payment_options($detail) {
	$out=array();
	if(shop_item_accepts_paypal($detail)) $out[] = "Paypal";
	if(shop_item_accepts_google_checkout($detail)) $out[] = "Google Checkout";
	if(shop_item_accepts_cheque($detail)) $out[] = "Cheque";
	if(shop_item_accepts_bank_transfer($detail)) $out[] = "Bank transfer";
	return $out;
}

function shop_item_google_checkout_merchant_id($detail="") {
	return "";	
}

function shop_item_paypal_merchant_id($detail="") {
	return "";
}

function shop_admin_configuration() {
	osc_plugin_configure_view(osc_plugin_path(__FILE__));
}

function shop_item_is_for_sale() {
	return (!osc_item_is_expired());

}




    

    /**
     * ADD HOOKS
     */
    osc_register_plugin(osc_plugin_path(__FILE__), 'shop_install');
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'shop_uninstall');
    osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'shop_admin_configuration');
   
   

    osc_add_hook('item_detail', 'shop_item_detail');
    osc_add_hook('front_item_form', 'shop_form');

    osc_add_hook('item_form', 'shop_form');
    osc_add_hook('item_tools', 'shop_item_tools');
    osc_add_hook('item_edit', 'shop_item_edit');
    osc_add_hook('item_form_post', 'shop_item_edit_post');
    osc_add_hook('item_edit_post', 'shop_item_edit_post');
    osc_add_hook('pre_item_post', 'shop_pre_item_post');
    

    osc_add_hook('delete_item', 'shop_delete_item');
    
    osc_add_hook('user_menu', 'shop_user_menu');
    osc_add_hook('user_register_form', 'shop_user_form');
    osc_add_hook('user_form', 'shop_user_form');
    
    osc_add_hook('cron_hourly', 'shop_calculate_scores');
    osc_add_hook('user_register_completed', 'shop_new_user');
    osc_add_hook('item_form_post', 'shop_new_item');
    osc_add_hook('search_conditions', 'shop_search_conditions');
      
?>
